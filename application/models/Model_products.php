<?php 

class Model_products extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/* get the brand data */
	public function getProductData($id = null)
	{
		if($id) {
			$sql = "SELECT p.*, sum(qty_remaining) as rem_qty FROM products p join products_inventory pi where p.id = ? and p.id = pi.p_id group by pi.p_id";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT p.*, sum(qty_remaining) as rem_qty FROM products p join products_inventory pi where p.id = pi.p_id group by pi.p_id ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getProductDataInventory()
	{
		$sql = "SELECT p.*, sum(qty_remaining) as rem_qty FROM products p join products_inventory pi where p.id = pi.p_id group by pi.p_id ORDER BY id DESC";
		$query = $this->db->query($sql);
		// echo '<pre>' ;
		// print_r($query->result());
	
		return $query->result();
	}
 
	public function getActiveProductData()
	{
		$sql = "SELECT * FROM products WHERE availability = ? ORDER BY id DESC";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
	}

	public function create($data , $data1)
	{
		$status = false;
		if($data and $data1) {
			$this->db->insert('products',$data);
			$inserted_product_id = $this->db->insert_id();		
			if ($inserted_product_id > 0)
			{
				$data1["p_id"] = $inserted_product_id ;
				$inserted_product_inventory_id = $this->db->insert('products_inventory', $data1);
				if ($inserted_product_inventory_id > 0)
				{
						$status = true;
				}
			}
		}	
		return $status;
		
	}

	public function getproducts(){
		$this->db->select('*');
		$this->db->from('products');
		$query=$this->db->get();
		if($query->num_rows()>0){
			return $query->result() ;
		}
		return false ;
	}


	public function getcategories(){
		$this->db->select('*');
		$this->db->from('categories');
		$query=$this->db->get();
		if($query->num_rows()>0){
			return $query->result();
		}
		return false ;
	}

	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('products', $data);
			return ($update == true) ? true : false;
		}
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('products');
			return ($delete == true) ? true : false;
		}
	}

	public function countTotalProducts()
	{
		$sql = "SELECT * FROM products";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}


   
	}