<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Inventory
      <small>Products</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Products</li>
    </ol>
  </section>

  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">
        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>


        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Add Inventory</h3>
          </div>
          <!-- /.box-header -->
          
            <form role="form" action="<?php echo base_url('products/productinventory') ?>" method="post">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Quantity<span style="color:red">*</span></th>
                        <th>Unit</th>
                        <th>Total Price<span style="color:red">*</span></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($inventoryproducts): ?>
                        <?php foreach($inventoryproducts as $inventoryproduct) : ?>
                        <tr>
                            <td><?php echo $inventoryproduct->name;?></td>
                            <td><input type="text" class="form-control"  name="qty[]" value='<?php echo $inventoryproduct->qty; ?>' placeholder="Enter Qty" autocomplete="off" /></td>
                            <td><?php echo $inventoryproduct->unit;?></td>
                            <td><input type="text" class="form-control"  name="price[]" value='<?php echo $inventoryproduct->price  ;?>' placeholder="Enter price" autocomplete="off" /></td>
                            <td><input type="hidden" name="product_id[]" value='<?php echo $inventoryproduct->id;?>' ></td>
                        </tr>
                        <?php endforeach ?>
                    <?php endif ; ?>
                </tbody>
            </table>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save Changes</button>
                    <a href="<?php echo base_url('products/') ?>" class="btn btn-warning">Back</a>
                </div>
            </form>
               
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>



</div>